# Contributing

Testing Locally:

```shell
asdf plugin test <plugin-name> <plugin-url> [--asdf-tool-version <version>] [--asdf-plugin-gitref <git-ref>] [test-command*]

#
asdf plugin test regula https://gitlab.com/bimsland/asdf-regula.git "regula version"
```

Tests are automatically run in GitLab CI on push and PR.
